import {connectRouter, routerMiddleware} from "connected-react-router";
import thunkMiddleware from "redux-thunk";
import axios from '../axios-api'
import {applyMiddleware, combineReducers, compose, createStore} from "redux";
import {createBrowserHistory} from "history";
import {loadState, saveState} from "./localStorage";
import userReducer from "./reducer/userReducer";
import placesReducer from "./reducer/placeReducer";
import commentsReducer from "./reducer/commentReducer";

export const history = createBrowserHistory();

const rootReducer = combineReducers({
    router: connectRouter(history),
    user: userReducer,
    places: placesReducer,
    comments: commentsReducer
});

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const middleware = [
    thunkMiddleware,
    routerMiddleware(history)

];

const enhancers = composeEnhancers(applyMiddleware(...middleware));

const persistedState = loadState();

const store = createStore(rootReducer, persistedState, enhancers);

store.subscribe(() => {
    saveState({
        user: {
            user: store.getState().user.user
        }
    });
});

axios.interceptors.request.use(config => {
    try {
        config.headers['Authorization'] = store.getState().user.user.token;
    } catch (e) {
        // do nothing, user is not logged in
    }

    return config;
});

export default store;
