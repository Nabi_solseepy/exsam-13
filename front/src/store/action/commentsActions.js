import axios from "../../axios-api";

export const FETCH_COMMENTS_SUCCESS = 'FETCH_COMMENTS_SUCCESS';

export const fetchCommentsSuccess  = (comments) => ({type: FETCH_COMMENTS_SUCCESS, comments});


export const fetchComments = (placeId) => {
    return dispatch => {
        let url = '/comments';
        if (placeId){
            url += `?placeId=${placeId}`
        }
        axios.get(url).then(
            response => {
                dispatch(fetchCommentsSuccess(response.data))
            }
        )
    }
};


export const addComment = (commentsData) => {
    return dispatch => {
        axios.post('/comments',  commentsData).then(
            () => {
                dispatch(fetchComments(commentsData.placeId))
            }
        )
    }
};




