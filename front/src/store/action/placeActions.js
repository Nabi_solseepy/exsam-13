import {push} from 'connected-react-router'
import axios from  '../../axios-api'
import {NotificationManager} from "react-notifications";


export const FETCH_PLACE = 'FETCH_PLACE';
export const CREATE_PLACES_SUCCESS = 'CREATE_PLACES_SUCCESS';

export const FETCH_ONE_PLACE = 'FETCH_ONE_PLACE';

export const DELETE_PLACE_SUCCESS = 'DELETE_PLACE_SUCCESS';

export const fetchPlaceSuccess = (places) => ({type: FETCH_PLACE, places});
export const createPlacesSuccess = () => ({type: CREATE_PLACES_SUCCESS});

export const fetchOnePlaceSuccess = (place) => ({type: FETCH_ONE_PLACE, place});

export const fetchPlace = () => {
    return dispatch => {

        axios.get('/places').then(
            response => {
                dispatch(fetchPlaceSuccess(response.data))
            }
        )
    }
};


export const createPlaces = (placesData) => {
    return dispatch => {
        axios.post('/places', placesData).then(
            () => {
                dispatch(createPlacesSuccess());
                NotificationManager.success('Вы успешно добавили фото');
                dispatch(push('/'))
            },
            error => {
                if (error.response) {
                    NotificationManager.error(error.response.data.message);
                }
            }
        )
    }
};

export const onePlace = (id) => {
    return dispatch => {
        return axios.get('/places/' + id ).then(
            response => {
                 dispatch(fetchOnePlaceSuccess(response.data))

            }
        )
    }
};
export const creatPlacePassage = (id, passage) => {
    return dispatch => {
        return  axios.post('/places/' + id, passage).then(
            () =>  {
                dispatch(onePlace(id));
                NotificationManager.success('Вы успешно добавили фото');
            }
        )
    }
};

export const deletePlace = (id) => {
    return dispatch => {
        axios.delete('/places/' + id).then(
            () => {
                dispatch(fetchPlace());
                    NotificationManager.success('Успешно удалено')

            }

        )
    }
}


