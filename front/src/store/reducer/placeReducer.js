import {FETCH_ONE_PLACE, FETCH_PLACE} from "../action/placeActions";

const initialState  = {
    places: [],
    place: {}

};

const placesReducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_PLACE:
            return {...state, places: action.places};
        case FETCH_ONE_PLACE:
            return {...state, place: action.place };
        default:
            return state
    }
};

export default placesReducer