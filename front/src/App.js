import React, {Component,Fragment} from 'react';
import './App.css';
import {Route} from "react-router-dom";
import {Container} from "reactstrap";
import Toolbar from "./component/UI/Toolbar/Toolbar";
import {connect} from "react-redux";
import {logoutUser} from "./store/action/userActions";
import Register from "./container/Register/Register";
import Login from "./container/Login/Login";
import {NotificationContainer} from "react-notifications";
import AddPlace from "./container/AddPlace/AddPlace";
import Place from "./container/Place/Place";
import OnePlace from "./container/OnePlace/OnePlace";


class App extends Component {
    render() {
        return (
            <Fragment>
                <NotificationContainer/>
                <Toolbar user={this.props.user}
                         logout={this.props.logoutUser}
                />
                <Container>
                    <Route path="/" exact component={Place}/>
                    <Route path="/register" exact component={Register}/>
                    <Route path="/login" exact component={Login}/>
                    <Route path="/AddNewPlace" exact component={AddPlace}/>
                    <Route path="/place/:id" exact component={OnePlace}/>
                </Container>
            </Fragment>
        );
    }
}


const mapStateToProps = state => ({
    user: state.user.user
});

const mapDispatchToProps = dispatch => ({
    logoutUser: () => dispatch(logoutUser())
});



export default connect(mapStateToProps, mapDispatchToProps)(App);
