import React from 'react';
import PropTypes from 'prop-types';
import {Col, FormFeedback, FormGroup, Input, InputGroup, Label, InputGroupAddon, InputGroupText} from "reactstrap";

const FormElement = ({propertyName, title, error, children, mask, valid, button, checkbox, checkBoxValue, onClick, ...props}) => {
    return (
        <FormGroup row>
            <Label sm={3} for={propertyName}>{title}</Label>
            <Col sm={9}>
                <InputGroup>
                    {checkbox ?
                        <InputGroupAddon addonType="prepend">
                            <InputGroupText>
                                <Label check>
                                    <Input addon type={checkbox} value={checkBoxValue} name="radio"/>
                                </Label>
                            </InputGroupText>
                        </InputGroupAddon>
                        : null}
                    <Input
                        name={propertyName} id={propertyName}
                        invalid={!!error} valid={valid}
                        mask={mask} onClick={onClick}
                        {...props}
                    />
                    {children}
                </InputGroup>
                {error && (
                    <FormFeedback>
                        {error}
                    </FormFeedback>
                )}
            </Col>
        </FormGroup>
    );
};

FormElement.propTypes = {
    propertyName: PropTypes.string.isRequired,
    title: PropTypes.string.isRequired,
    mask: PropTypes.string,
    valid: PropTypes.bool,
    error: PropTypes.string,
};

export default FormElement;
