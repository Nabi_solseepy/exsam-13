import React, {Fragment} from 'react';
import {Nav, Navbar, NavbarBrand} from "reactstrap";
import {NavLink as RouterNavLink} from "react-router-dom";
import UserMenu from "./Menu/UseMenu";
import AnonymousMenu from "./Menu/AnonymousMenu";



const Toolbar = ({user, logout}) => {
    return (
        <Fragment>
            <Navbar color="light" light expand="md">
                <NavbarBrand to="/" tag={RouterNavLink} >Cafe Critic</NavbarBrand>
                <Nav className="ml-auto" navbar>
                    {user ? <UserMenu user={user} logout={logout}/>: <AnonymousMenu/>}

                </Nav>
            </Navbar>
        </Fragment>
    );
};

export default Toolbar;