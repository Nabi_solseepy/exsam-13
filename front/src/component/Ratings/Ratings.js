import React, {Fragment} from 'react';
import {Col, Row} from "reactstrap";
import StarRatings from "react-star-ratings";

const Ratings = (props) => {
    return (
            <Fragment>
                <hr/>
                <Row>
                    <Col sm={2}>
                        Overall
                    </Col>
                    <Col sm={10}>
                        <StarRatings
                            rating={props.Overal}
                            ratingToShow={props.Overal}
                            starDimension={'25px'}
                            starRatedColor="orange"
                            numberOfStars={5}
                        />
                        <span style={{display: 'inline-block', marginLeft: '20px', verticalAlign: 'text-top'}}>
                                 {props.Overal}
                   </span>
                    </Col>
                    <Col sm={2}>
                        Quality of food
                    </Col>
                    <Col sm={10}>
                        <StarRatings
                            rating={props.qualityOfFood}
                            ratingToShow={props.qualityOfFood}
                            starDimension={'25px'}
                            starRatedColor="orange"
                            numberOfStars={5}
                        />
                        <span style={{display: 'inline-block', marginLeft: '20px', verticalAlign: 'text-top'}}>
                                 {props.qualityOfFood}
                </span>
                    </Col>

                    <Col sm={2}>
                        Service quality
                    </Col>
                    <Col sm={10}>
                        <StarRatings
                            rating={props.ServiceQuality}
                            ratingToShow={props.ServiceQuality}
                            starDimension={'25px'}
                            starRatedColor="orange"
                            numberOfStars={5}
                        />
                        <span style={{display: 'inline-block', marginLeft: '20px', verticalAlign: 'text-top'}}>
                                 {props.ServiceQuality}
                </span>
                    </Col>
                    <Col sm={2}>
                        Interior
                    </Col>
                    <Col sm={10}>
                        <StarRatings
                            rating={props.interior}
                            ratingToShow={props.interior}
                            starDimension={'25px'}
                            starRatedColor="orange"
                            numberOfStars={5}
                        />
                        <span style={{display: 'inline-block', marginLeft: '20px', verticalAlign: 'text-top'}}>
                                 {props.interior}
                </span>
                    </Col>
                </Row>
            </Fragment>
    );
};

export default Ratings;