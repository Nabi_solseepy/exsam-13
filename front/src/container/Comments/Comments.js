import React, {Component, Fragment} from 'react';
import {Button, Card, CardText, CardTitle, Col, FormGroup, Row} from "reactstrap";
import StarRatings from "react-star-ratings";
import {connect} from "react-redux";
import { fetchComments} from "../../store/action/commentsActions";
import Ratings from "../../component/Ratings/Ratings";

class Comments extends Component {


    componentDidMount() {
        this.props.fetchComments(this.props.placeId)
    }

    render() {
        let qualityOfFood = 0;
        let  ServiceQuality = 0;
        let interior = 0;
        let Overal = 0;

        {this.props.comments.map(comment => {
            qualityOfFood += comment.qualityOfFood / this.props.comments.length;
            ServiceQuality += comment.ServiceQuality / this.props.comments.length;
            interior += comment.interior / this.props.comments.length;
            Overal = (qualityOfFood + ServiceQuality + interior) / 3
        })}
        return (
            <Fragment>
                <Ratings
                    Overal={parseFloat(Overal.toFixed(1))}
                    qualityOfFood={parseFloat(qualityOfFood.toFixed(1))}
                    ServiceQuality={parseFloat(ServiceQuality.toFixed(1))}
                    interior={parseFloat(interior.toFixed(1))}
                />
                <h3>Comments</h3>
                {this.props.comments.length > 0 ?  (
                    <div style={{height: '500px', overflow: 'auto'}}>
                        {this.props.comments.map(comment => (
                            <Card key={comment._id} className="mt-2 p-3">


                                <CardTitle>
                                    On {comment.dateTime}: <span>{comment.user.username}</span>
                                </CardTitle>
                                <CardText>text: {comment.text}</CardText>


                                {comment.ServiceQuality ? (


                                    <Row>
                                        <Col sm={2}>
                                                Quality of food
                                        </Col>

                                        <Col sm={10}>
                                            <StarRatings
                                                rating={comment.qualityOfFood}
                                                ratingToShow={comment.qualityOfFood}
                                                starDimension={'25px'}
                                                starRatedColor="orange"
                                                numberOfStars={5}
                                            />

                                            <span style={{display: 'inline-block', marginLeft: '20px', verticalAlign: 'text-top'}}>
                                 {comment.qualityOfFood}
                             </span>
                                        </Col>
                                        <Col sm={2}>
                                            Service quality
                                        </Col>

                                        <Col sm={10}>
                                            <StarRatings
                                                rating={comment.ServiceQuality}
                                                ratingToShow={comment.ServiceQuality}
                                                starDimension={'25px'}
                                                starRatedColor="orange"
                                                numberOfStars={5}
                                            />

                                            <span style={{display: 'inline-block', marginLeft: '20px', verticalAlign: 'text-top'}}>
                                 {comment.ServiceQuality}
                             </span>
                                        </Col>
                                        <Col sm={2}>
                                            Interior
                                        </Col>
                                        <Col sm={10}>
                                            <StarRatings
                                                rating={comment.interior}
                                                ratingToShow={comment.interior}
                                                starDimension={'25px'}
                                                starRatedColor="orange"
                                                numberOfStars={5}
                                            />

                                            <span style={{display: 'inline-block', marginLeft: '20px', verticalAlign: 'text-top'}}>
                                 {comment.interior}
                             </span>
                                        </Col>
                                    </Row>
                                ): null}


                            </Card>
                        ))}
                    </div>
                ):(
                    <div>
                        <span>No Comments</span>
                    </div>
                )}
            </Fragment>
        );
    }
}

const mapStateToProps = (state) => ({
    comments: state.comments.comments,
});

const mapDispatchToProps = dispatch => ({
    fetchComments: (recipeId) => dispatch(fetchComments(recipeId)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Comments);