import React, {Component} from 'react';
import Container from "reactstrap/es/Container";
import FormElement from "../../component/UI/Form/FormElement";
import {Button, Col, Form, FormGroup} from "reactstrap";
import {connect} from "react-redux";
import {registerUser} from "../../store/action/userActions";

class Register extends Component {
    state = {
        username: '',
        password: ''
    };

    inputChangeHandler = event => {
        this.setState({
            [event.target.name]: event.target.value
        })
    };

    submitFromHandler = event => {
        event.preventDefault();

        this.props.registerUser({...this.state})
    };

    render() {
        return (
            <Container>
                <Form onSubmit={this.submitFromHandler}>
                    <FormElement
                        propertyName="username"
                        title="Имя"
                        type="text"
                        value={this.state.username}
                        onChange={this.inputChangeHandler}
                        placeholder="Введите имя"

                    />

                    <FormElement
                        propertyName="password"
                        title="Пароль"
                        type={"password"}
                        value={this.state.password}
                        onChange={this.inputChangeHandler}
                        placeholder="Введите пароль"
                    />
                    <FormGroup row>

                        <Col sm={{offset: 3, size: 9}}>
                            <Button type="submit" color="primary">
                                Зарегистрироваться
                            </Button>

                        </Col>
                    </FormGroup>

                </Form>
            </Container>
        );
    }
}


const mapStateToProps = state => ({
    error: state.user.registerError
});

const mapDispatchToProps = dispatch => ({
    registerUser: (userData) => dispatch(registerUser(userData))
});

export default connect(mapStateToProps, mapDispatchToProps)(Register);