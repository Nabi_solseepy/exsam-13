import React, {Component, Fragment} from 'react';
import {Button, Col, Form, Input, Label, Row} from "reactstrap";
import {connect} from "react-redux";
import {addComment} from "../../store/action/commentsActions";
import FormElement from "../../component/UI/Form/FormElement";


const rankNumber = [
    '5.0',
    '4.0', '4.1', '4.2', '4.3', '4.4', '4.5', '4.6','4.7','4.8', '4.9',
    '3.0', '3.1', '3.2', '3.3', '3.4', '3.5', '3.6','3.7','3.8', '3.9',
    '2.0', '2.1', '2.2', '2.3', '2.4', '2.5', '2.6','2.7','2.8', '2.9',
    '1.0', '1.1', '1.2', '1.3', '1.4', '1.5', '1.6','1.7','1.8', '1.9',
];
class AddComments extends Component {
    state = {
        text: '',
        qualityOfFood: '',
        ServiceQuality: '',
        interior: '',
        placeId: this.props.placeId
    };

    inputChangeHandler = event => {
        this.setState({
            [event.target.name]: event.target.value
        })
    };

    submitFormHandler = event => {
        event.preventDefault();

        this.props.addComment({...this.state})

    };
    render() {
        return (
            <Fragment>
                <Form onSubmit={this.submitFormHandler}>
                    <hr/>
                    <FormElement
                        type="textarea"
                        propertyName="text"
                        title="Add a comment:"
                        value={this.state.text}
                        onChange={this.inputChangeHandler}
                    />

                    <Row>
                        <Col sm={3}>
                            <Label>Quality of food </Label>
                            <Input
                                type="select"
                                name="qualityOfFood"
                                onChange={this.inputChangeHandler}

                            >
                                {rankNumber.map(number => (
                                    <option key={number} value={number}>{number}</option>
                                ))}
                            </Input>
                        </Col>
                        <Col sm={3}>
                            <Label>Service quality</Label>
                            <Input
                                type="select"
                                name="ServiceQuality"
                                onChange={this.inputChangeHandler}
                            >
                                {rankNumber.map(number => (
                                    <option key={number} value={number}>{number}</option>
                                ))}
                            </Input>
                        </Col>
                        <Col sm={3}>
                            <Label>Interior</Label>

                            <Input
                                type="select"
                                name="interior"
                                onChange={this.inputChangeHandler}
                            >
                                {rankNumber.map(number => (
                                    <option key={number} value={number}>{number}</option>
                                ))}
                            </Input>
                        </Col>
                        <Col sm={3} className="p-4 mt-1">
                            <Button type="submit" color="primary">
                                Add
                            </Button>
                        </Col>
                    </Row>
                </Form>
            </Fragment>
        );
    }
}




const mapDispatchToProps = dispatch => ({
    addComment: (commentsData) => dispatch(addComment(commentsData))
});

export default connect(null, mapDispatchToProps)(AddComments);