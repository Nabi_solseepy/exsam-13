import React, {Component, Fragment} from 'react';
import {connect} from "react-redux";
import FormElement from "../../component/UI/Form/FormElement";
import {Button, Col, Form, FormGroup} from "reactstrap";
import {loginUser} from "../../store/action/userActions";

class Login extends Component {

    state = {
        username: '',
        password: ''
    };


    inputChangeHandler = event => {
        this.setState({
            [event.target.name]: event.target.value
        })
    };

    submitFromHandler = event => {
        event.preventDefault();
        this.props.loginUser({...this.state})
    };

    render() {
        return (
            <Fragment>
                <Form onSubmit={this.submitFromHandler}>
                    <FormElement
                        propertyName="username"
                        title="Имя"
                        type="text"
                        value={this.state.username}
                        onChange={this.inputChangeHandler}
                        placeholder="Введите имя"
                    />
                    <FormElement
                        propertyName="password"
                        title="Пароль"
                        type="password"
                        value={this.state.password}
                        onChange={this.inputChangeHandler}
                        placeholder="Введите пароль"
                        autoComplete="new-password"
                    />

                    <FormGroup row>
                        <Col sm={{offset: 3, size: 9}}>
                            <Button type="submit" color="success">
                                Войти
                            </Button>
                        </Col>
                    </FormGroup>
                </Form>

            </Fragment>
        );
    }
}


const mapStateToProps = state => ({
    error: state.user.loginError
});

const mapDispatchToProps = dispatch => ({
    loginUser: userData => dispatch(loginUser(userData))
});

export default connect(mapStateToProps, mapDispatchToProps)(Login);