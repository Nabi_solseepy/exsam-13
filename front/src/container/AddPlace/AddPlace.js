import React, {Component} from 'react';
import {Button, Col, Form, FormGroup, Input, Label} from "reactstrap";
import {connect} from "react-redux";
import {createPlaces} from "../../store/action/placeActions";


class AddRecipe extends Component {
    state = {
        title: '',
        image: '',
        description: '',
        agreement: "false"
    };

    inputCheckboxHandler = event => {
        let agreement = this.state.agreement;

        if (event.target.checked) {
            agreement = event.target.value
        } else {
            agreement = null
        }

        this.setState({ agreement: agreement });
    };
    inputChangeHandler = event => {
        this.setState({
            [event.target.name]: event.target.value})
    };

    fileChangeHandler = event => {
        this.setState({
            [event.target.name]: event.target.files[0]
        })
    };

    submitFormHandler = event => {
        event.preventDefault();
        const formData = new FormData();

        Object.keys(this.state).forEach(key =>{
            formData.append(key, this.state[key])
        });
        this.props.createPlaces(formData);

    };

    render() {
        return (
            <Form onSubmit={this.submitFormHandler}>
                <FormGroup row>
                    <Label for="exampleEmail" sm={2}>Title</Label>
                    <Col sm={10}>
                        <Input type="text" name="title" placeholder="Title" onChange={this.inputChangeHandler} />
                    </Col>
                </FormGroup>
                <FormGroup row>
                    <Label for="examplePassword" sm={2}>description</Label>
                    <Col sm={10}>
                        <Input name="description" placeholder="description" type="textarea" onChange={this.inputChangeHandler}/>
                    </Col>
                </FormGroup>
                <FormGroup row>
                    <Label for="examplePassword" sm={2}>Image</Label>
                    <Col sm={10}>
                        <Input  type="file" name="image"   placeholder="image" onChange={this.fileChangeHandler}/>
                    </Col>
                </FormGroup>
                <FormGroup row>
                    <Col sm={{offset: 2, size: 10}}>
                        <Button type="submit" color="primary">
                            Add Recipe
                        </Button>
                    </Col>
                </FormGroup>
                <FormGroup check>
                    <Label check>
                        <Input  onChange={this.inputCheckboxHandler } value={"true"} type="checkbox" />{' '}
                        Вы принемаете соглашение
                    </Label>
                </FormGroup>
            </Form>
        );
    }
}


const mapStateToProps = (state) => ({
    places: state.user.places
});

const mapDispatchToProps = dispatch => ({
    createPlaces: (placesData) => dispatch(createPlaces(placesData))
});

export default connect(mapStateToProps, mapDispatchToProps )(AddRecipe);