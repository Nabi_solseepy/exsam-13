import React, {Component, Fragment} from 'react';
import {Button, Card, CardBody, CardColumns, CardImg, CardTitle, Col, FormGroup} from "reactstrap";
import {deletePlace, fetchPlace} from "../../store/action/placeActions";
import {connect} from "react-redux/es/alternate-renderers";
import {NavLink} from "react-router-dom";

class Place extends Component {

    componentDidMount() {
        this.props.fetchPlace()
    }

    render() {
        return (
            <Fragment>
                <CardColumns>
                    {this.props.places.map(place => (


                        <Card key={place._id}>
                            <NavLink to={`/place/${place._id}`}>
                                <CardImg top width="100%" src={"http://localhost:8000/upload/" + place.image}
                                         alt="Card image cap"/>

                                <CardBody>
                                    <CardTitle>{place.title}</CardTitle>
                                </CardBody>
                            </NavLink>
                            {this.props.user && this.props.user.role === 'admin' ? (
                                <FormGroup row>
                                    <Col sm={{offset: 1, size: 7}}>
                                        <Button onClick={() => this.props.deletePlace(place._id)}
                                                color="danger">удалить</Button>
                                    </Col>
                                </FormGroup>
                            ) : null}
                        </Card>
                    ))}
                </CardColumns>
            </Fragment>


        )
    }
}

const mapStateToProps = (state) => ({
    places: state.places.places,
    user: state.user.user
});

const mapDispatchToProps = dispatch => ({
    fetchPlace : () => dispatch(fetchPlace()),
    deletePlace: (id) => dispatch(deletePlace(id))
});

export default connect(mapStateToProps,mapDispatchToProps)(Place);