import React from 'react';
import Slider from "react-slick"
import './OnePlace.css'
import './SlickCarousel.css'
const SlickCarousel = ({place}) => {
    const settings = {
        dots: true,
        infinite: false,
        speed: 500,
        slidesToShow: 3,
        slidesToScroll: 1
    };
    return (
        <div className="gallery-slider">
            <Slider {...settings}>
                {place.passage && place.passage.map(slider => (
                    <img key={slider} className='Slide-img' src={"http://localhost:8000/upload/"  + slider} alt={place.title}/>
                ))}
            </Slider>
        </div>
    );
};

export default SlickCarousel;