import React, {Component,Fragment} from 'react';
import {creatPlacePassage, onePlace} from "../../store/action/placeActions";
import {Button, CardImg, CardText, Col, Form, FormGroup, Row} from "reactstrap";
import {connect} from "react-redux/es/alternate-renderers";
import FormElement from "../../component/UI/Form/FormElement";
import SlickCarousel from "./SlickCarousel";
import Comments from "../Comments/Comments";
import AddComments from "../AddComments/AddComments";


class OnePlace extends Component {


    state = {
        passage: []
    };

    passageChangeHandler = event => {
        event.preventDefault();

        let files = Array.from(event.target.files);

        files.forEach((file) => {
            let reader = new FileReader();
            reader.onloadend = () => {
                this.setState({
                    passage: [...this.state.passage, file],
                });
            };
            reader.readAsDataURL(file);
        });
    };

    submitFormHandler = event => {
        event.preventDefault();
        const formData = new FormData();

        console.log(formData, 'formData');

        for (let i = 0; i < this.state.passage.length; i++) {
            formData.append('passage', this.state.passage[i]);
        }

        this.props.creatPlacePassage(this.props.match.params.id, formData);
    };

    componentDidMount() {
        this.props.onePlace(this.props.match.params.id)
    }

    render() {
        return (
            <Fragment>
                <hr/>
                <Row>
                    <Col sm={8}>
                        <h3>{this.props.place.title}</h3>
                        <CardText>{this.props.place.description}</CardText>

                    </Col>
                    <Col sm={4}>
                        <CardImg top width="100%" src={"http://localhost:8000/upload/" + this.props.place.image} alt="Card image cap" />
                    </Col>
                </Row>

                <hr/>
                <SlickCarousel place={this.props.place}/>
                <Comments placeId={this.props.match.params.id}/>
                {this.props.user && this.props.user._id !== this.props.place.user &&  <AddComments placeId={this.props.match.params.id}/>}
                {!!this.props.user   ? (
                <Fragment>
                    <Form className="pt-3" onSubmit={this.submitFormHandler}>
                        <hr/>
                        <h3>Add a photo</h3>
                        <FormElement
                            type="file"
                            propertyName="passage"
                            title="Images"
                            id="passage"
                            multiple
                            onChange={this.passageChangeHandler}
                        />
                        <FormGroup row>
                            <Col sm={{offset: 3, size: 10}}>
                                <Button type="submit" color="primary">
                                    Upload
                                </Button>
                            </Col>
                        </FormGroup>
                    </Form>
                </Fragment>
                ) : null}
            </Fragment>

        );
    }
}

const mapStateToProps = (state) => ({
    place: state.places.place,
    user: state.user.user
});

const mapDispatchToProps = dispatch => ({
    onePlace: (placeId) => dispatch(onePlace(placeId)),
    creatPlacePassage: (id ,passage) => dispatch(creatPlacePassage(id, passage))
});


export default connect(mapStateToProps,mapDispatchToProps)(OnePlace);