const express = require('express');
const config = require('../config');
const auth = require('../meddlware/auth');
const permit = require('../meddlware/permit')

const path = require('path');
const multer = require('multer');
const nanoid = require('nanoid');
const Place = require('../models/Place');
const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname));
    }
});

const upload = multer({storage});


const router = express.Router();

router.get('/', async (req, res) => {
    try {
        const places = await Place.find();
        return res.send(places)
    }catch (e) {
        return res.status(500)
    }
});
router.get('/:id', async (req, res) => {
    try {
        const places = await Place.findById(req.params.id);

        return res.send(places)
    }catch (e) {
        return res.status(500)
    }
});

router.post('/', auth, upload.single('image'), async(req, res) => {
    try {
        const placeData = req.body;

        console.log(placeData, 'recipeData');

        if (placeData.agreement === "false"){
            return res.status(400).send({message:"не приняли соглашения" })
        }

        if (req.file) {
            placeData.image = req.file.filename
        }
        placeData.user = req.user._id;


        const place = await Place(placeData);

        await place.save();

        return res.send(place);

    } catch (error) {
        console.log(error);
        return res.status(400).send(error)
    }
});


router.post('/:id', auth, upload.array('passage'), async (req, res) => {

    const place = await Place.findById(req.params.id);
    if (req.files) {
        req.files.map(file => place.passage.push(file.filename))

    }

    place.save()
        .then((result) => res.send(result))
        .catch(error => res.status(400).send(error));
});


router.delete('/:id', auth, permit('admin'), async (req, res) => {
    try {
        const place =  await Place.findByIdAndDelete({_id: req.params.id});

        await place.save();

        return res.send(place)

    } catch (e) {
        res.status(500).send(e)
    }

});



module.exports = router;