const express = require('express');


const router = express.Router();


router.get('/:id', async (req, res) => {
    console.log(req.params);
    try {
        const user = await User.findOne({_id: req.params.id});
        return res.send(user)
    }catch (e) {
        return res.status(500).send(e)
    }
});


router.get('/', async (req, res) => {
    try{
        const users = await User.find();
        return res.send(users)
    }catch (e) {
        return res.status(500).send(e)
    }

});


router.post('/', async (req, res) => {
    try {
        const user = new User({
            username: req.body.username,
            password: req.body.password
        });

        user.generateToken();

        await user.save();
        return res.send({message: 'User registered', user});
    } catch (error) {
        return res.status(400).send(error)
    }

});

router.post('/sessions', async (req, res) => {
    const user = await User.findOne({username: req.body.username});


    if (!user) {
        return res.status(400).send({error: 'User does not exist'});
    }

    const isMatch = await user.checkPassword(req.body.password);

    if (!isMatch) {
        return res.status(400).send({error: 'Password incorrect'});
    }

    user.generateToken();

    await user.save();

    res.send({message: 'Login successful', user})

});


router.delete('/sessions', async (req, res) => {
    const token = req.get('Authorization');
    const success = {message: 'Logged out'};
    if (!token) {
        return res.send(success);

    }
    const user = await User.findOne({token});

    if (!user){
        return res.send(success)
    }

    user.generateToken();
    await user.save();

    return res.send(success)
});const User = require('../models/User');

const auth = async (req, res, next) => {
    const token = req.get('Authorization');

    if (!token) {
        return res.status(401).send({error: 'Token not provided'});
    }

    const user = await User.findOne({token});

    if (!user) {
        return res.status(401).send({error: 'Token incorrect'});
    }

    req.user = user;

    next();
};

module.exports = auth;




module.exports = router;