const mongoose = require('mongoose');
const Schema = mongoose.Schema;


const CommentSchema = new Schema({
    user: {
        type: Schema.Types.ObjectId,
        ref: 'User',
        required: true
    },
    placeId: {
        type: Schema.Types.ObjectId,
        ref: 'User',
        required: true
    },
    text: {
        type: String,
        required: true
    },
    qualityOfFood:{
        type: Number
    },
    ServiceQuality:{
        type: Number
    },
    interior:{
        type: Number
    },
    dateTime: {
        type: String
    }
});



const Comment = mongoose.model('Comment', CommentSchema);

module.exports = Comment;