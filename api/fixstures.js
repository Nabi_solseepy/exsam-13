const mongoose = require('mongoose');
const config = require('./config');
const nanoid = require("nanoid");

const User = require('./models/User');
const Place = require('./models/Place')

const run = async () => {
    await mongoose.connect(config.dbUrl, config.mongoOptions);

    const connection = mongoose.connection;

    const collections = await connection.db.collections();

    for (let collection of collections) {
        await collection.drop();
    }


    const user = await  User.create(
        {
            username: 'Alex Kim',
            password: '123',
            token: nanoid()
        },
        {
            username: 'John Doe',
            password: '123',
            role: 'admin',
            token: nanoid()
        }
    );
    await Place.create(
        {
            user: user[0]._id,
            title: 'restorane',
            image: 'star.jpg',
            description: 'Рестораны классифицируются или различаются по-разному. Основными факторами обычно являются сама еда (например, вегетарианская, морепродукты, стейк); кухни (например, итальянская, китайская, японская, индийская, французская, мексиканская, тайская) или заведение (например, тапас-бар, суши-бар,'
        },

        {
            user: user[0]._id,
            title: 'Japan',
            image: 'japan.jpeg',
            description: '\n' +
                '\n' +
                'Содержание\n' +
                '1\tНазвание\n' +
                '2\tИстория происхождения суши\n' +
                '3\tВиды суши\n' +
                '4\tИнгредиенты\n' +
                '4.1\tРис для суши\n' +
                '4.2\tНори\n' +
                '4.3\tНачинки\n' +
                '4.4\tПриправы\n' +
                '5\tСуши в мире\n' +
                '6\tЭкологические проблемы, связанные с суши\n' +
                '7\tРиски употребления суши\n' +
                '8\tГалерея\n' +
                '9\tСм. также\n' +
                '10\tПримечания\n' +
                'Название\n' +
                '\n' +
                'Суши, готовые к употреблению.\n' +
                'В словарях встречаются два варианта названия блюда — «суши» и «суси»[2]. На сегодня более распространено в русском языке написание и произношение «суши»[3], заимствованное при посредстве '
        },
    );


    return connection.close();
};


run().catch(error => {
    console.error('Something wrong happened...', error);
});
