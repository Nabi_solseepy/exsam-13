const path = require('path');

const rootPath = __dirname;

module.exports = {
    rootPath,
    uploadPath: path.join(rootPath, 'public/upload'),
    dbUrl: 'mongodb://localhost/cafe',
    mongoOptions: {
        useNewUrlParser: true,
        useCreateIndex: true
    }
};
